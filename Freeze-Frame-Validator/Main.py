import os,requests
import subprocess
import ffmpeg

class Operation:

    def download(self, url):
        get_response = requests.get(url, allow_redirects=True)
        file_name = url.split("/")[-1]
        open(file_name, 'wb').write(get_response.content)
        return file_name

    def run_freezedetect(self, fileName):
        command = 'ffmpeg -i ' + fileName + ' -vf "freezedetect=n=0.003:d=0.1" -map 0:v:0 -f null -'
        output = subprocess.getoutput(command)
        clear_lines = []
        for line in output.split('\n'):
            if "[freezedetect" in line:
                clear_lines.append(line)
        return clear_lines

    def buildSeriesPoints(self, freeze_detect_output):
        valid_periods = []
        last_freeze_start = 0
        longest_valid_period = 0
        total_period = 0
        precentage = 0
        output_video = {}

        for line in freeze_detect_output:
            current_time = line.split(' ')[-1]
            current_operation = line.split(' ')[-2].split('.')[-1]  # freeze_start/ freeze_duration/ freeze_end
            if current_operation == 'freeze_start:':
                last_freeze_start = round(float(current_time), 2)
            if current_operation == 'freeze_end:':
                period = round(float(current_time), 2) - last_freeze_start
                total_period += period
                precentage = (total_period/round(float(current_time), 2))*100
                if longest_valid_period < period:
                    longest_valid_period = period
                valid_periods.append([last_freeze_start, round(float(current_time), 2)])
        output_video["longest_valid_period"] = longest_valid_period
        output_video["valid_video_percentage"] = round(float(precentage), 2)
        output_video["valid_periods"] = valid_periods

        return output_video


    def main(self, url_list):
        output_json = {}
        videos = []
        file_names = []

        for url in url_list:
            file_names.append(self.download(url))

        for file in file_names:
            videos.append(self.buildSeriesPoints(self.run_freezedetect(file)))
        output_json["all_videos_freeze_frame_synced"]= "true"
        output_json["videos"] = videos

        print(output_json)
        return output_json



